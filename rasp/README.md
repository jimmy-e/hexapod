This script controls data receiving and sending between an external web server, a xbox 
controller, an arduino uno and a bluno nano (arduino nano with integrated bluetooth).
The script can easily be configured to other setups, as long as you fix carefully step 5. 

Everything is done via threading, where the threads don't affect each other (i.e a error caused by 
the xbox controller doesn't effect the reading from the website).


1. Plug your xbox wifi adapter into an USB port on the Raspberry

2. Run 'sudo apt-get install xboxdrv' and reboot

3. If not already, install Python 

4. Extract everything under Raspberry/Python/ into some folder on the rasp

5. Open read_xbox.py (nano read_xbox.py) and change to your own
- IP (of the server computer)
- Serial ports (USB for Arduino Uno and Arudino Bluno)

6. Check that you website is up on running on http://X.X.X.X/Spyder/control.php, where 
X.X.X.X is the IP of your host computer (which is running the server). 

7. Run ./start.sh

