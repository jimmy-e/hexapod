import xbox
import threading
import time
import serial
import urllib2
import struct



global SLEEP_TIME
global XBOX
global result
global result2
global IP
global port

SLEEP_TIME = 0.05
XBOX = 1
IP = "5.5.5.6"#"127.0.0.1"
xVal = 127
yVal = 127
xValw = 127
yValw = 127
port = '/dev/ttyUSB0'
port2 = '/dev/ttyACM0'
heading = "180.00, 20, 2.60"
radar_array = []
for i in range(151):
	radar_array = radar_array + [[i + 15,50]]



def packC(val):
	return struct.pack('c', val)

def packB(value):
    	return struct.pack('B', value)

def xbox_read(): # reads events from the controller, updating result
	global xVal
	global yVal
	global XBOX
	x = 127
	y = 127
	for event in xbox.event_stream(deadzone=12000):
		value = (int(event.value) + 2**15) >> 8
		value = max(value-1, 0)
		key = event.key[0]

		if key == "X":
			xVal = value
		if key == "Y":
			yVal = value
		if event.key == "RT":
			XBOX = 1


def website_read():
	earlier = ""
	global xValw
	global yValw
	global XBOX
	while 1:
		try:
			response = urllib2.urlopen("http://" + IP + "/Spyder/PHP_get_control.php")# "/html/spyderny/Spyder/PHP_get_control.php")
			status = response.read()
			status = status.replace("|", "")
			if status == earlier: 
				time.sleep(0.5)
				continue
			if int(status[0]):
				XBOX = 0
			else:
				XBOX = 1

			if int(status[1]):
				xValw = 127
				yValw = 255
			elif int(status[2]):	
				xValw = 127
				yValw = 0
			elif int(status[3]): 
				xValw = 255
				yValw = 127
			elif int(status[4]):			
				xValw = 0
				yValw = 127
			else:
				xValw = 127
				yValw = 127
			earlier = status
		except urllib2.HTTPError, e:
			print e.code
		except urllib2.URLError, e:
			print e.args
		time.sleep(0.5)

		
def write(): # writing the result
	ser = serial.Serial(port)
	ser.baudrate = 9600

	while 1:
		try:
			if XBOX:
				ser.write(packC('A'))
				ser.write(packB(xVal))
				ser.write(packB(yVal))
				
			else:	
				ser.write(packC('A'))	
				ser.write(packB(xValw))
				ser.write(packB(yValw))
		except KeyboardInterrupt:
			ser.close()

                time.sleep(SLEEP_TIME)
		

def read(): # reads the serial write from arduino
	global radar_array
	global heading
	ser1 = serial.Serial(port)
	ser2 = serial.Serial(port2)
	ser2.readline()
	while 1:
		try:
			line = ser2.readline()
			print ser1.readline()

		except:
			continue
		print len(line)
		if "!" in line:
			print line
			line_array = line.split(';')
			try:
				radar_array[int(line_array[0]) - 15][1] = int (line_array[1])
				heading = str(line_array[2])
			except:
				continue
		time.sleep(SLEEP_TIME)
			
def web_write():
	while 1:
		radar = ""
		for i in range(151):
			radar = radar + str(radar_array[i][0]) + "," + str(radar_array[i][1]) + ';'
		url1 = 'http://' + IP +'/Spyder/PHP_give_temp_optical_output.php'+'?'+str(heading)# 'html/spyderny/Spyder/PHP_give_temp_optical_output.php'+'?'+str(heading)
		#print url1
		req = urllib2.Request(url1)
		response = urllib2.urlopen(req)
		the_page = response.read()
		
		url2 = 'http://' + IP + '/Spyder/PHP_give_radar_output.php' + '?' + str(radar)# '/html/spyderny/Spyder/PHP_give_radar_output.php' + '?' + str(radar)
		#print url2
		req = urllib2.Request(url2)
		response = urllib2.urlopen(req)
		the_page = response.read()
		time.sleep(0.1)


 
	
			
def main(): # starting all the threads
	t1 = threading.Thread(target = xbox_read)
	t2 = threading.Thread(target = write)
	t3 = threading.Thread(target = read)
	t4 = threading.Thread(target = website_read)
	t5 = threading.Thread(target = web_write)
	t1.daemon = True # kills the thread when main is killed
	t2.daemon = True
	t3.daemon = True
	t4.daemon = True
	t5.daemon = True
	t1.start()
	t2.start()
	t3.start()
	t4.start()
	t5.start()

	while 1:
		time.sleep(10)


if __name__ == '__main__':
	main()
