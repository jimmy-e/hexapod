from_website = 0;
document.getElementById("start").innerHTML = "TOGGLE CONTROL";

function updateClock() {
    var now = new Date();
        months = ['Jan', 'Feb', 'Mar', 'April', 'May', 'June','Jule', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']; // you get the idea
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var seconds = now.getSeconds();// again, you get the idea
        if (hours < 10) {hours = '0' + hours;}
        if (minutes < 10) {minutes = '0' + minutes;}
        if (seconds < 10) {seconds = '0' + seconds;}
        var time = hours + ':' + minutes + ':' + seconds;

        // a cleaner way than string concatenation
        date = [now.getDate(), 
                months[now.getMonth()],
                now.getFullYear()].join(' ');

    // set the content of the element with the ID time to the formatted string
    document.getElementById('time').innerHTML = [date, time].join(' / ');

    // call this function again in 1000ms (taken away some time for processing)
    setTimeout(updateClock, 995);
}



function write_to_file(index) {
	if(index == -1){
		if (from_website == 0){
			from_website = 1;
			document.getElementById("start").innerHTML = "END CONTROL";
      var index = 0;
		}
		else {
			from_website = 0;
			document.getElementById("start").innerHTML = "TOGGLE CONTROL";
      var index = 0;
      
		}
	}
  if (from_website == 0){
    var inputs = ["|0|0|0|0|0|","|0|1|0|0|0|","|0|0|1|0|0|","|0|0|0|1|0|","|0|0|0|0|1|"];
  }
  else {
    var inputs = ["|1|0|0|0|0|","|1|1|0|0|0|","|1|0|1|0|0|","|1|0|0|1|0|","|1|0|0|0|1|"];
  }
  var steerings = ["Currently not steering!", "Currently steering Front!","Currently steering Back!","Currently steering Right!","Currently steering Left!"];
  var current_input = inputs[index];
  var current_steering = steerings[index];

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      if (from_website == 0){ 

         document.getElementById("current_action_print").innerHTML = "";
      }
      else{
      document.getElementById("current_action_print").innerHTML = current_steering;
    }
    }
  };
  xhttp.open("POST", "PHP_give_user_input.php?" + current_input, true);
  xhttp.send();
}

function loadDoc() {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {

    	var txt = xhttp.responseText;
    	array = txt.split(";");

    }
  };
  xhttp.open("POST", "radar_output.txt", true);
  xhttp.send();
}


function temp() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
        ind = xhttp.responseText.search(",");

      document.getElementById("temperature").innerHTML = xhttp.responseText.slice(0,ind)+ " °";
      document.getElementById("optical").innerHTML = xhttp.responseText.slice(ind+1, xhttp.responseText.slice.lenght) + " \u03BCW/cm<sup>2</sup>";
    }
  };
  xhttp.open("POST", "temp_optical_output.txt", true);
  xhttp.send();
  setTimeout(temp, 2000);
}



var line = {
	x: 200,
	y: 200,
	length: 200,
	angle: 335*2/360*Math.PI,
	speed: Math.PI *1.5/ 360.,
	end: {
		x: 200,
		y: 0
	}
}

line.draw = function(){
	this.angle += this.speed;
	if (this.angle <= 195*2/360*Math.PI ) {
    this.angle = 195*2/360*Math.PI;
    this.speed = -this.speed;
  }
  if (this.angle >= 345*2/360*Math.PI ) {
    this.angle = 345*2/360*Math.PI;
    this.speed = -this.speed;
  }
  this.end.x = this.x + this.length * Math.cos(this.angle);
	this.end.y = this.y + this.length * Math.sin(this.angle);
	ctx.save();
	ctx.strokeStyle = "#383";
	ctx.lineWidth = 4;
	ctx.beginPath();
	ctx.moveTo(this.x,this.y);
	ctx.lineTo(this.end.x,this.end.y);
	ctx.stroke();
  var index = Math.round(this.angle*360/(2*Math.PI)) -195;

  if (deg_array[index] != 0) {
    new Blip(deg_array[index].x, deg_array[index].y, 0.2 );
  }
  return index;

	//var i = deg_array.length;
	/*while(i--){
    if (deg_array[i] == 0) {continue;}
		if (pointToLineDistance(line, line.end, deg_array[i]  ) < 1.) {
			new Blip(deg_array[i].x, deg_array[i].y, 0.2 );

		}
	}
*/
      
  

	ctx.restore();
}


function Ball(x,y,distance, r){
  this.x = x;
  this.y = y;
  this.distance = distance;
  this.r = r;
}
Ball.all = [];
Ball.prototype = {
  draw: function(){
    ctx.save();
      ctx.translate(this.x,this.y);
      ctx.fillStyle = "#fb0";
      ctx.beginPath();
      ctx.arc(0,0, this.r, 0, Math.PI * 2, true);
      ctx.closePath();
      ctx.fill();
    ctx.restore();
  },
  remove: function(){
    Ball.all.splice(Ball.all.indexOf(this), 1);
  }
};
function Blip(x,y,t){
	this.x = x;
	this.y = y;
	this.t = t;
	Blip.all.push(this);
}
Blip.all = [];


function pointToLineDistance(A, B, P){
	var normalLength = Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
	return Math.abs((P.x - A.x) * (B.y - A.y) - (P.y - A.y) * (B.x - A.x)) / normalLength;
}
function lineDistance(A, B ){ 
    return Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
}


var canvas = document.getElementById("radar");
var ctx = canvas.getContext('2d');
ctx.fillStyle = "white";
ctx.fillRect(0, 0, canvas.width, canvas.height);
array = "";
last = "";
length = "";
// Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {

} else {
  alert('The File APIs are not fully supported by your browser.');
}


deg_array = Array.apply(null, Array(165-15+1)).map(Number.prototype.valueOf,0);






function radar() {
  // Clear display
   
    ctx.save();
    ctx.fillStyle = "rgba(20, 0, 0, .04)";
    ctx.beginPath();
    ctx.arc(200, 200, 200, 0, Math.PI , true);
    ctx.closePath();
    ctx.fill();
    ctx.restore();


  var current_angle = line.draw();
  if (current_angle == 0 || current_angle == 45 || current_angle == 90 ||current_angle == 135 ||  current_angle == 150) {
    last = array.slice();

    for(var i = 0; i < 150; i+=6){
         //document.getElementById("demo").innerHTML = array[i];
          var numbers = array[i].split(",");
          var r = Number(numbers[1]);  
          var angle = 180 - Number(numbers[0]);
          var theta = 2*Math.PI /360 * (angle+180);
          var ball = new Ball(200 + r*Math.cos(theta), 200 + r*Math.sin(theta),Number(numbers[1]), 2);
          deg_array.splice(angle-15, 1, ball);
    }

  }
  var i = Blip.all.length;
  var kill_cutoff = 0.1;
  var blip_strength_drain = 0.997;
  while(i--){
    	ctx.save();
  	    if (Blip.all[i].t > kill_cutoff){
  	    Blip.all[i].t *= blip_strength_drain;
  	    var col = "rgba(25, 255, 25, " + Blip.all[i].t + ")"
		    ctx.fillStyle = col;
		    ctx.beginPath();
		    ctx.arc(Blip.all[i].x, Blip.all[i].y, 2, 0, Math.PI * 2, true);
		    ctx.closePath();
		    ctx.fill();
    	}
    	else if (Blip.all[i].t <= kill_cutoff){
    		Blip.all.splice(i,1);
    	}
    	ctx.restore();
    }
  if (current_angle < 85){
    var str = "0";
  } else {
    var str = ""
  }
  var print_angle = str.concat((current_angle+15).toString());

    var distance = deg_array[current_angle].distance.toFixed(0);
  if (distance == 0) {
    var print_dis = "00";
  } else {
    var print_dis = distance.concat(" cm");
  }
  
  var p = document.getElementById("angle").innerHTML = print_angle;//(current_angle+15).toString();
  document.getElementById("distance").innerHTML =print_dis;

    ctx.strokeStyle = "rgba(80,80,80, 1)";
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.arc(200, 200, 200, 0, Math.PI , true);
    ctx.closePath();
    ctx.stroke();

    for (var i = 1; i < 5; i++){
	    ctx.strokeStyle = "rgba(30,80,30, 0.5)";
	    ctx.lineWidth = 1;
	    ctx.beginPath();
	    ctx.arc(200, 200, 40 * i, 0, Math.PI , true);
	    ctx.closePath();
	    ctx.stroke();

    }
        for (var j = 0; j <6; j++){
        var angle = Math.PI+ j*Math.PI/6;
	    ctx.strokeStyle = "rgba(30,80,30, 0.5)";
	    ctx.lineWidth = 1;
	    ctx.beginPath();		
	    ctx.moveTo(200,200);//this.x,this.y);
		ctx.lineTo(200+ 200* Math.cos(angle),200 + 200 * Math.sin(angle));
		ctx.stroke();
		//window.alert(Math.cos(angle));



    }

} 

updateClock(); // initial call
loadDoc();
setInterval( loadDoc, 50);

setInterval(radar, 8);
