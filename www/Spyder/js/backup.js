

var array;
function loadDoc() {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {

    	var txt = xhttp.responseText;
    	var array = txt.split("\n");

    }
  };
  xhttp.open("GET", "temp.txt", true);
  xhttp.send();
}
function test(){
	return 2
}
function temp() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
        ind = xhttp.responseText.search(",");

      document.getElementById("temperature").innerHTML = xhttp.responseText.slice(0,ind)+ "°C";
      document.getElementById("optical").innerHTML = xhttp.responseText.slice(ind+1, xhttp.responseText.slice.lenght);
    }
  };
  xhttp.open("POST", "temp_and_optical.txt", true);
  xhttp.send();
  setTimeout(temp, 2000);
}



var line = {
	x: 200,
	y: 200,
	length: 200,
	angle: 3/2*Math.PI,
	speed: Math.PI *1.5/ 360.,
	end: {
		x: 200,
		y: 0
	}
}

line.draw = function(){
	this.angle += this.speed;
	if (this.angle > 1.9*Math.PI 
		|| this.angle < 1.1*Math.PI){
		this.speed = -this.speed
	}
    this.end.x = this.x + this.length * Math.cos(this.angle);
	this.end.y = this.y + this.length * Math.sin(this.angle);
	ctx.save();
	ctx.strokeStyle = "#383";
	ctx.lineWidth = 2;
	ctx.beginPath();
	ctx.moveTo(this.x,this.y);
	ctx.lineTo(this.end.x,this.end.y);
	ctx.stroke();


	var i = Ball.all.length;
	while(i--){
		if (pointToLineDistance(line, line.end, Ball.all[i] ) < 1.
			&& lineDistance(line, Ball.all[i] ) < line.length
			&& lineDistance(line.end, Ball.all[i] ) < line.length){
			new Blip( Ball.all[i].x, Ball.all[i].y, 0.2 );

		}
	}
	ctx.restore();
}


function Ball(x,y,r){
  this.x = x;
  this.y = y;
  this.r = r;
  this.vx = 0;
  this.vy = 0;
  Ball.all.push(this);
}
Ball.all = [];
Ball.prototype = {
  draw: function(){
    ctx.save();
      ctx.translate(this.x,this.y);
      ctx.fillStyle = "#fb0";
      ctx.beginPath();
      ctx.arc(0,0, this.r, 0, Math.PI * 2, true);
      ctx.closePath();
      ctx.fill();
    ctx.restore();
  },
  remove: function(){
    Ball.all.splice(Ball.all.indexOf(this), 1);
  }
};
function Blip(x,y,t){
	this.x = x;
	this.y = y;
	this.t = t;
	Blip.all.push(this);
}
Blip.all = [];


function pointToLineDistance(A, B, P){
	var normalLength = Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
	return Math.abs((P.x - A.x) * (B.y - A.y) - (P.y - A.y) * (B.x - A.x)) / normalLength;
}
function lineDistance(A, B ){ 
    return Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
}

var array = "";
var canvas = document.getElementById("radar");
var ctx = canvas.getContext('2d');
ctx.fillStyle = "white";
ctx.fillRect(0, 0, canvas.width, canvas.height);

// Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {

} else {
  alert('The File APIs are not fully supported by your browser.');
}







function radar() {
  // Clear display
    ctx.save();
    ctx.fillStyle = "rgba(20, 0, 0, .04)";
    ctx.beginPath();
    ctx.arc(200, 200, 200, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fill();
    ctx.restore();



  // Update balls. Add balls if under 200 exist
  /*if (Ball.all.length < 20){
    for(var i = 0; i < 5; i++){
      var ball = new Ball(Math.random()*400,Math.random()*400,2);
      ball.vx = Math.random() / 100;
      ball.vy = Math.random() / 100;
    }
  }*/
  

  for(var i = 0; i < array.length; i++){
  	window.alert(array);

  		var numbers = array[i].split(",");
  		window.alert(numbers[0]);
  	  var ball = new Ball(Number(numbers[0]),Number(numbers[1]));
      ball.vx = 0;
      ball.vy = 0;

  }
  
  var i = Ball.all.length;
  while(i--){
	  // Update ball
	  Ball.all[i].x += Ball.all[i].vx;
	  Ball.all[i].y += Ball.all[i].vy;
	  if (Ball.all[i].x > canvas.width - Ball.all[i].r) {
	    Ball.all[i].x = canvas.width - Ball.all[i].r;
	    Ball.all[i].vx = -Math.abs(Ball.all[i].vx);
	  }
	  else if (Ball.all[i].x < Ball.all[i].r) {
	    Ball.all[i].x = Ball.all[i].r;
	    Ball.all[i].vx = Math.abs(Ball.all[i].vx);
	  }
	  if (Ball.all[i].y > canvas.height - Ball.all[i].r) {
	    Ball.all[i].y = canvas.height - Ball.all[i].r;
	    Ball.all[i].vy = -Math.abs(Ball.all[i].vy);
	  }
	  else if (Ball.all[i].y < Ball.all[i].r) {
	    Ball.all[i].y = Ball.all[i].r;
	    Ball.all[i].vy = Math.abs(Ball.all[i].vy);
	  }
  }

    var i = Blip.all.length;
    document.getElementById("demo").innerHTML = i;
    //console.log(i + " blips");
    var kill_cutoff = 0.0005;
    var blip_strength_drain = 0.997;
    while(i--){
    	ctx.save();
  	    if (Blip.all[i].t > kill_cutoff){
  	    	Blip.all[i].t *= blip_strength_drain;
  	    	var col = "rgba(25, 255, 25, " + Blip.all[i].t + ")"
  	    	//var col = "rgba(25, 255, 25, 0.2 )"
			// ctx.strokeStyle = col;
			// ctx.beginPath();
			// ctx.moveTo(Blip.all[i].x, Blip.all[i].y);
			// ctx.lineTo(Blip.all[i].x+1, Blip.all[i].y+1);
			// ctx.stroke();

		    ctx.fillStyle = col;
		    ctx.beginPath();
		    ctx.arc(Blip.all[i].x, Blip.all[i].y, 2, 0, Math.PI * 2, true);
		    ctx.closePath();
		    ctx.fill();
    	}
    	else if (Blip.all[i].t <= kill_cutoff){
    		Blip.all.splice(i,1);
    	}
    	ctx.restore();
    }


	line.draw();

    ctx.strokeStyle = "rgba(80,80,80, 1)";
    ctx.lineWidth = 2;
    ctx.beginPath();
    ctx.arc(200, 200, 200, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.stroke();

    for (var i = 1; i < 5; i++){
	    ctx.strokeStyle = "rgba(30,80,30, 0.5)";
	    ctx.lineWidth = 1;
	    ctx.beginPath();
	    ctx.arc(200, 200, 40 * i, 0, Math.PI * 2, true);
	    ctx.closePath();
	    ctx.stroke();

    }
        for (var j = 0; j <12; j++){
        var angle = Math.PI+ j*Math.PI/6;
	    ctx.strokeStyle = "rgba(30,80,30, 0.5)";
	    ctx.lineWidth = 1;
	    ctx.beginPath();		
	    ctx.moveTo(200,200);//this.x,this.y);
		ctx.lineTo(200+ 200* Math.cos(angle),200 + 200 * Math.sin(angle));
		ctx.stroke();
		//window.alert(Math.cos(angle));



    }
} 
setInterval( radar, 1000/80 );

setInterval( loadDoc, 500 / 80);