1. Download and extract Spyder/

2. Download XAMPP https://www.apachefriends.org/download.html

3. Start Xampp Control Panel and start Apache by clicking 'Start'

4. Test writing localhost/ in your web browser. If you are NOT ending up on the xampp dashboard, check previous steps again. 

5. Put your extracted Spyder folder under xampp/htdocs, on Windows probably 'C:\xampp\htdocs\Spyder'

6. By writing http://localhost/Spyder/control.php you will come to the control system site. If you want to look at the other files also, 
just write http://localhost/Spyder/

7. If you want to see the control system on another computer (on the same wifi) just put your local IP instead of 'localhost', 
i.e. http://5.5.5.6/Spyder/control.php

8. Enjoy!
