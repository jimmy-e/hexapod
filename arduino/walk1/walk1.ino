
#include <Servo.h>

#define RUNSTEP 5
#define RUNDELAY 16
#define RUNLIMIT 110
#define CLIMBSTEP 3
#define CLIMBDELAY 10
#define CLIMBLIMIT 140
#define HORIZANGLE 18
float STEP = RUNSTEP;
float TSTEP;
int STEPDELAY = RUNDELAY;
int MODE = 0;
float UPLIMIT = RUNLIMIT;
float DOWNLIMIT = 90;
float BACKLIMIT = 90-HORIZANGLE;
float FORWARDLIMIT = 90+HORIZANGLE;
int beginQuadLeft = -1;
bool passedLeft = false;int beginQuadRight = -1;
bool passedRight = false;

typedef struct {          //Defines one leg
  Servo l;                //Lateral servo
  Servo h;                //Height servo
  float la,ha;          // angles
  byte state;
} Leg;

#define BUSY 0
#define READY 1
#define GROUND 2

void setupLeg(Leg &leg, int hpin, int lpin) { //Attaches leg to its pins. Hpin for height servo etc.
  leg.la = 90 ; leg.ha = DOWNLIMIT;
  byte state = BUSY;         
  leg.h.attach(hpin);
  leg.l.attach(lpin);
  leg.h.write(leg.ha);
  leg.l.write( 90 );

}

Leg leg1;
Leg leg2;
Leg leg3;
Leg leg4;
Leg leg5;
Leg leg6;

Leg leftGroup[] = { leg1, leg4, leg5 }; //Groups are defined as two legs on one side and one on the other
Leg rightGroup[] = { leg2, leg3, leg6 };
int b[2] = {127, 127};

void forwardLeg(Leg &leg,Leg & leg2,float vel) { //Walks a leg one step. CounterLeg used to check if there is enough support
  float k = (FORWARDLIMIT-BACKLIMIT)/(UPLIMIT-DOWNLIMIT);
  float Y1 = (leg.la-BACKLIMIT)/k+DOWNLIMIT; float Y2 = (BACKLIMIT-leg.la)/k+UPLIMIT;
  if(Y1 <= leg.ha && Y2 >= leg.ha){ // at the back, going up  
    if(vel < 0 || leg2.state == GROUND){
      leg.ha+=vel; leg.la = BACKLIMIT; leg.state = BUSY;
    }else{
      leg.ha = DOWNLIMIT; leg.la = BACKLIMIT; leg.state = GROUND; 
    }
  }else if(Y1<=leg.ha && Y2<= leg.ha){  // at the top, going forward
    leg.la+=vel; leg.ha = UPLIMIT; leg.state = BUSY;
  }else if(Y1>=leg.ha && Y2<= leg.ha){  // at the front, going down
    if(vel > 0 || leg2.state == GROUND){
      leg.ha-=vel; leg.la = FORWARDLIMIT; leg.state = BUSY;
    }else{
      leg.ha = DOWNLIMIT; leg.la = FORWARDLIMIT; leg.state = GROUND; 
    }
  }else if(Y1>=leg.ha && Y2>= leg.ha){  // on the ground, going back
    leg.la-=vel; leg.ha = DOWNLIMIT; leg.state = GROUND;
  }
}


void moveGroup(Leg * legArr,Leg * legArr2,float steer,float vel){
  if(steer > 0){ FORWARDLIMIT = 90+HORIZANGLE;BACKLIMIT = 90-HORIZANGLE;TSTEP = vel;}else{FORWARDLIMIT = 90-HORIZANGLE*steer;BACKLIMIT = 90+HORIZANGLE*steer; TSTEP = -vel*steer;} 
  forwardLeg(legArr[0],legArr2[0],TSTEP);
  forwardLeg(legArr[2],legArr2[2],TSTEP);
  if(steer < 0){ FORWARDLIMIT = 90+HORIZANGLE;BACKLIMIT = 90-HORIZANGLE;TSTEP = vel;}else{FORWARDLIMIT = 90+HORIZANGLE*steer;BACKLIMIT = 90-HORIZANGLE*steer;TSTEP = vel*steer; }
  forwardLeg(legArr[1],legArr2[1],-TSTEP);
}

void centerGroup(Leg * legArr) {   //Sets a leg to the center
  for (unsigned int i = 0; i < 3; i++) {
    legArr[i].la = 90;
    legArr[i].ha = (DOWNLIMIT);
  }
}

void turnGroup(Leg * legArr,Leg * legArr2,float vel) {      //Used when the spider is turning
  FORWARDLIMIT = 90+HORIZANGLE;BACKLIMIT = 90-HORIZANGLE;UPLIMIT = RUNLIMIT;
  forwardLeg(legArr[0],legArr2[0],vel);
  forwardLeg(legArr[1],legArr2[1],vel);
  forwardLeg(legArr[2],legArr2[2],vel);
}


void setLegFront(Leg &leg) {   //Sets leg in starting position
  leg.la = (90+HORIZANGLE);
  leg.ha = (DOWNLIMIT);
}

void setLegBack(Leg &leg) {    //Not used ATM
  leg.la =(90-HORIZANGLE);
  leg.ha =(DOWNLIMIT);
}

void setGroupFront(Leg * legArr) { //Sets legs in starting position
  setLegFront(legArr[0]);
  setLegBack(legArr[1]);
  setLegFront(legArr[2]);
}

void setGroupBack(Leg * legArr) {  //Not used ATM
  setLegBack(legArr[0]);
  setLegFront(legArr[1]);
  setLegBack(legArr[2]);
}

void updatePositions(Leg * legArr){
  for(int i = 0;i<3;i++){
    legArr[i].l.write(legArr[i].la);
    legArr[i].h.write(legArr[i].ha);
  }
}



void setup() {
  Serial.begin(9600); Serial.setTimeout(20);
  setupLeg(leg1, 12, 13); //Front left, h, l
  setupLeg(leg2, 10, 11); //Front right, h, l
  setupLeg(leg3, 8, 9); //Middle left, h, l
  setupLeg(leg4, 6, 7); //Middle right, h, l
  setupLeg(leg5, 4, 5);//Back left, h, l
  setupLeg(leg6, 2, 3);//Back right, h, l
  setGroupFront(leftGroup);  setGroupFront(rightGroup);
  updatePositions(leftGroup); updatePositions(rightGroup);

}

float x=0,y=0;
unsigned long currentTime = 0;
unsigned long time2 = 0;
int z = 0;

float radar = 15; int rdir = 1;
void loop() {

  currentTime = millis();

  if (Serial.available() > 2 && Serial.read() == 'A') {

        b[0] = Serial.read();
        b[1] = Serial.read();
  
        x = -( (((float)b[0])/127.0)-1.0 );
        
        y = -( (((float)b[1])/127.0)-1.0 );

        time2 = currentTime;
      }
    
    Serial.print(b[0]);
    Serial.print(b[1]);
    Serial.print("\n");
    
    if(currentTime-time2 > 600){
        setGroupFront(leftGroup);
        setGroupFront(rightGroup);
        x = 0; y = 0;
    }

  
  if(x*x+y*y > 0.01){   // any motion
    if(y*y > 0.1){
      if(abs(y) > 0.75){STEP = y*RUNSTEP;UPLIMIT = RUNLIMIT;}else{STEP = y*CLIMBSTEP;UPLIMIT = CLIMBLIMIT;}
   
      if(abs(x) < 0.15){x = 0;}else{x = (x>0)?0.66:-0.66;}
      float X = (x>0)?(1-x):(-1-x);
      //X = (y>0)?X:(-X);
      moveGroup(leftGroup,rightGroup,-X,STEP);
      moveGroup(rightGroup,leftGroup,X,-STEP);
    }else{
      if(x*x > 0.01){
        STEP =x*RUNSTEP; 
        turnGroup(leftGroup,rightGroup,STEP);
        turnGroup(rightGroup,leftGroup,STEP);
      }
    }
  }else{
      centerGroup(leftGroup); centerGroup(rightGroup);
  }
  
  updatePositions(leftGroup); updatePositions(rightGroup);
  delay(STEPDELAY);
}




 
