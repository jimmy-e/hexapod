#include <Servo.h>
#include <IRremote.h>

#define FORWARDLIMIT 105  //Maximum forward "degree" for all legs
#define BACKLIMIT 75      //Maximum backwards "degree" for all legs
#define UPLIMIT 85       //Maximum upwards "degree" for all legs
#define DOWNLIMIT 55      //Maximum downwards "degree" for all legs
#define STEP 3            //number of degrees that the legs are mover per iteration
#define STEPDELAY 6       //Millis to wait after legs have been "walked" one step
#define IR_PIN 14
#define UP 1
#define DOWN 0


typedef struct {          //Defines one leg
  Servo l;                //Lateral servo
  Servo h;                //Height servo
} Leg;

Leg setupLeg(Leg leg, int hpin, int lpin) { //Attaches leg to its pins. Hpin for height servo etc.
  
  leg.h.attach(hpin);
  leg.l.attach(lpin);
  leg.h.write(DOWNLIMIT);
  leg.l.write( (FORWARDLIMIT + BACKLIMIT) / 2 );

  return leg;
}

IRrecv irrecv(IR_PIN);
decode_results results;
unsigned short timer;       //Used to make continuous movements from discrete IR-signal
unsigned short currMillis;  //Used to make continuous movements from discrete IR-signal
int go;                     //Used to make continuous movements from discrete IR-signal
unsigned long dir;          //Used to make continuous movements from discrete IR-signal

Leg leg1;
Leg leg2;
Leg leg3;
Leg leg4;
Leg leg5;
Leg leg6;

Leg leftGroup[] = { leg1, leg4, leg5 }; //Groups are defined as two legs on one side and one on the other
Leg rightGroup[] = { leg2, leg3, leg6 };

void forwardLeg(Leg* leg, Leg* counterLeg) { //Walks a leg one step. CounterLeg used to check if there is enough support
  int lv = leg->l.read();
  int hv = leg->h.read();

  int st = DOWN;

  for (unsigned int i = 0; i < 3; i++) {
    if (counterLeg[i].h.read() > DOWNLIMIT)
      st = UP;
  }

  if ((lv <= BACKLIMIT && hv < UPLIMIT) && st == DOWN) {
    leg->h.write(hv + STEP);
  }else if (lv < FORWARDLIMIT && hv >= UPLIMIT) {
    leg->l.write(lv + STEP);
  }else if (hv > DOWNLIMIT && lv >= FORWARDLIMIT) {
    leg->h.write(hv - STEP);
  }else if (lv > BACKLIMIT && hv <= DOWNLIMIT) {
    leg->l.write(lv - STEP);
  }
}

void reverseLeg(Leg* leg, Leg* counterLeg) { //Other direction...
  int lv = leg->l.read();
  int hv = leg->h.read();

  int st = DOWN;

  for (unsigned int i = 0; i < 3; i++) {
    if (counterLeg[i].h.read() > DOWNLIMIT)
      st = UP;
  }

  if (lv < FORWARDLIMIT && hv <= DOWNLIMIT) {
    leg->l.write(lv + STEP);
  }else if ((lv >= FORWARDLIMIT && hv < UPLIMIT) && st == DOWN) { 
    leg->h.write(hv + STEP);
  }else if (hv >= UPLIMIT && lv > BACKLIMIT) {
    leg->l.write(lv - STEP);
  }else if (lv <= BACKLIMIT && hv > DOWNLIMIT) {
    leg->h.write(hv - STEP);
  }
}

void centerGroup(Leg* legArr) {   //Sets a leg to the center
  for (unsigned int i = 0; i < 3; i++) {
    legArr[i].l.write( (FORWARDLIMIT + BACKLIMIT) / 2);
    legArr[i].h.write(DOWNLIMIT);
  }
}

void moveS(Leg* group1, Leg* group2, float fact) {   //Calls the appropriate functions for the legs in the input group when moving forward


  if (fact < 0) {
    forwardLeg(&group1[0], group2);
    reverseLeg(&group1[1], group2);
    forwardLeg(&group1[2], group2);
    reverseLeg(&group2[0], group1);
    forwardLeg(&group2[1], group1);
    reverseLeg(&group2[2], group1);
  }else{
    reverseLeg(&group1[0], group2);
    forwardLeg(&group1[1], group2);
    reverseLeg(&group1[2], group2);
    forwardLeg(&group2[0], group1);
    reverseLeg(&group2[1], group1);
    forwardLeg(&group2[2], group1);
  }

}


void turnS(Leg* group1, Leg* group2, float fact) {      //Used when the spider is turning

  if (fact < 0) {
    forwardLeg(&group1[0], group2);
    forwardLeg(&group1[1], group2);
    forwardLeg(&group1[2], group2);
    forwardLeg(&group2[0], group1);
    forwardLeg(&group2[1], group1);
    forwardLeg(&group2[2], group1);
  }else{
    reverseLeg(&group1[0], group2);
    reverseLeg(&group1[1], group2);
    reverseLeg(&group1[2], group2);
    reverseLeg(&group2[0], group1);
    reverseLeg(&group2[1], group1);
    reverseLeg(&group2[2], group1);
  }
}

void setLegFront(Leg* leg) {   //Sets leg in starting position
  leg->l.write(FORWARDLIMIT);
  leg->h.write(DOWNLIMIT);
}

void setLegBack(Leg* leg) {    //Not used ATM
  leg->l.write(BACKLIMIT);
  leg->h.write(DOWNLIMIT);
}

void setGroupFront(Leg* legArr) { //Sets legs in starting position
  setLegFront(&legArr[0]);
  setLegBack(&legArr[1]);
  setLegFront(&legArr[2]);
}

void setGroupBack(Leg* legArr) {  //Not used ATM
  setLegBack(&legArr[0]);
  setLegFront(&legArr[1]);
  setLegBack(&legArr[2]);
}



void setup() {
  Serial.begin(9600);
  setupLeg(leg1, 12, 13); //Front left, h, l
  setupLeg(leg2, 10, 11); //Front right, h, l
  setupLeg(leg3, 8, 9); //Middle left, h, l
  setupLeg(leg4, 6, 7); //Middle right, h, l
  setupLeg(leg5, 4, 5);//Back left, h, l
  setupLeg(leg6, 2, 3);//Back right, h, l
  timer = 0;
  irrecv.enableIRIn(); //Enables IR reception
  setGroupFront(leftGroup);  setGroupFront(rightGroup);
}

float x = 0, y = 0, del = 0;
unsigned long currentTime = 0;
unsigned long time2 = 0;
byte b[2] = { 127, 127 };

void loop() {

  currMillis = (unsigned short) millis();

  if (irrecv.decode(&results)) {

    if (results.value != 0xFFFFFFFF) {    //IR sends 0xFFFFFFFF as "repeat"-signal
      dir = results.value;
        
    }else{
      go = 1;
      timer = (unsigned short) millis();
    }
    
    irrecv.resume();

    
  } else {
    if (currMillis - timer > 100)       //Stop walking if no signal for 100 millis
      go = 0;
  }

  if (go) {
    switch (dir) {
      case 0xFF629D :                   //Forward-arrow on remote
        moveS(leftGroup, rightGroup, 1);
        break;

      case 0xFFA857 :                   //Backward-arrow on remote
        moveS(leftGroup, rightGroup, -1);
        break;

      case 0xFFC23D :                   //Left-arrow on remote
        turnS(leftGroup, rightGroup, 1);
        break;

      case 0xFF22DD :                   //Right-arrow on remote
        turnS(leftGroup, rightGroup, -1);
        break;
        
    }
  } else if (currMillis - timer > 120) { //Set legs to starting position if no signal for 500 millis
      setGroupFront(leftGroup);
      setGroupFront(rightGroup);
  }

  delay(STEPDELAY);
}
