The folder sensors/ contains the code for obtaining and forwarding sensor data from the auxillary arduino.

Walk1 is the primary algorithm that works with analog steering over USB serial.

Walk2 is the older walking sequence that works with an IR controller. It depends on an IR library that is NOT included with the Arduino IDE. This library is available https://github.com/z3t0/Arduino-IRremote.git.
