#include <Arduino.h>
#include <Servo.h>
#include <Wire.h>//for communicating with I2C
#include <math.h>//for math functions such as atan
#include <HMC5883L_Simple.h>//this has to be found on the web

Servo myServo;
int distance;
long duration;
//double limit=200;
//double timeout=limit*2/(0.034); //200cm
const int trigPin=2; //TODO: Bluno
const int echoPin=3; //TODO: Bluno

HMC5883L_Simple Compass;


void setup() {
  myServo.attach(12);
  pinMode(trigPin,OUTPUT);
  pinMode(echoPin,INPUT);
  Serial.begin(9600);
  Wire.begin();
  Compass.SetDeclination(8, 33, 'E');
  Compass.SetScale(COMPASS_SCALE_130);
  Compass.SetOrientation(COMPASS_HORIZONTAL_X_NORTH);
  myServo.write(90);
    
  delay(1000);
}

void loop() {
  for(int i=15;i<165;i=i+6){
    myServo.write(i);
    delay(180);
    printDistance(i);
    printCompass();
    Serial.println("!");
  }
  for(int i=165;i>15;i=i-6){
    myServo.write(i);
    delay(180);
    printDistance(i);
    printCompass();
    Serial.println("!");
  }

}

void printDistance(int i){
  digitalWrite(trigPin,LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  duration=pulseIn(echoPin,HIGH,11764);//timeout 11764 microseconds, 200cm
  distance=duration*0.034/2;
  if(distance>200){
    Serial.print(i);
    Serial.print(";");
    Serial.print(200);
  }else{
    Serial.print(i);
    Serial.print(";");
    Serial.print(distance);
  }
  
}


void printCompass(){
  float heading = Compass.GetHeadingDegrees();
  Serial.print(";");
  Serial.print( heading );
}




